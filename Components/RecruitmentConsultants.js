import React from 'react'
import { View, Text, FlatList, Image ,StyleSheet, TouchableOpacity } from 'react-native'

const RecruitmentConsultants = () => {
    
    const DATA = [
        {
            id: 1,
            image: require("../assets/Recruitment4.jpg"),
        },
        {
            id: 2,
            image: require("../assets/Recruitment3.jpg"),
        },
        {
            id: 3,
            image: require("../assets/Recruitment2.jpg"),
        },
        {
            id: 4,
            image: require("../assets/Recruitment1.jpg"),
        }

    ]
    return (
        <View style = {styles.mainView}>
          
            <FlatList
            data = {DATA}
            keyExtractor = {(item) => item.id}
            numColumns = {2}
            renderItem = {({item})=>(
                <View style = {styles.imageView}>
                    <Image
                    source = {item.image}
                    style = {styles.image}
                    />
                </View>
            )}
            />
            <View style = {styles.buttonView}>
                    <TouchableOpacity >
                    <Text style  ={styles.viewMore}>
                    View More
                    </Text>
                    </TouchableOpacity>    
            </View>
        </View>
    )
}

const styles = StyleSheet.create({

    mainView : {
   
      backgroundColor : '#fff'
    },
  imageView: {
    margin: 10,
    flexDirection: "row",
    alignItems : "center"
  },
  image: {
    width: 160,
    height: 110,
  },
  buttonView : {
      alignSelf : 'flex-end',
      marginRight : 10,
  
  },
  viewMore : {
      color : '#FF0000',
      fontSize : 15, 
      fontWeight : 'bold',
      marginTop : 10,
      marginBottom : 10,
  
  }
});

export default RecruitmentConsultants
