import React, { Component } from 'react';
import { View, Image, StyleSheet, Dimensions,Animated } from 'react-native';
import { PinchGestureHandler, State } from 'react-native-gesture-handler';
const width = Dimensions.get("window").width;
const height = Dimensions.get("window").height;

// export default class PinchToZoom extends Component {
//   baseScale = new Animated.Value(1);
//   pinchScale = new Animated.Value(1);
  
//   scale = Animated.multiply(this.baseScale, this.pinchScale);
//   lastScale = 1;
//   onPinchGestureEvent = Animated.event(
//     [{ nativeEvent: { scale: this.pinchScale } }],
//     { useNativeDriver: true }
//   );
//   onPinchHandlerStateChange = (event) => {
//     if (event.nativeEvent.oldState === State.ACTIVE) {
//       this.lastScale *= event.nativeEvent.scale;
//       this.baseScale.setValue(this.lastScale);
//       this.pinchScale.setValue(1);
//     }
//   };
//   render() {
//     return (
      
//     );
//   }
// }

const PinchToZoom = (props) => {
  baseScale = new Animated.Value(1);
  pinchScale = new Animated.Value(1);
  lastScale = 1;
  
  
  let scale = new Animated.Value(1);

  const onZoomEvent = Animated.event(
    [
      {
        nativeEvent: { scale: scale }
      }
    ],
    {
      useNativeDriver: true
    }
  )

  const onZoomStateChange = event => {
      if (event.nativeEvent.oldState === State.ACTIVE) {
        // Animated.spring(scale, {
        //   toValue: 1,
        //   useNativeDriver: true
        // }).start()
      lastScale *= event.nativeEvent.scale;
      baseScale.setValue(lastScale);
      pinchScale.setValue(1);
      }
    }
  return (
    <PinchGestureHandler
        onGestureEvent={onZoomEvent}
        onHandlerStateChange={onZoomStateChange}
      >
        <Animated.Image
          source={{ uri: props.uri }}
          resizeMode={"contain"}
          style={[
            styles.pinchableImage,
            {
              transform: [{ perspective: 1 }, { scale: scale }],
            },
          ]}
        ></Animated.Image>
      </PinchGestureHandler>
  )
}

export default PinchToZoom


const styles = StyleSheet.create({
  pinchableImage: {
    position:'absolute',
    width: width,
    height: width,
  },
});