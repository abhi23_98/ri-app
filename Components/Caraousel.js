import React, { useEffect, useState  } from 'react'
import { View, Dimensions, FlatList, StyleSheet, Image, Text, TouchableOpacity, SafeAreaView} from 'react-native'
import Video from 'expo-av';
import { ScrollView } from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import { Entypo } from '@expo/vector-icons';
import PinchToZoom from "../Components/PinchToZoom";




const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;



const Caraousel = ({ navigation }) => {
    const [loading, setLoading] = useState()
    const [topItems, setTopItems] = useState();
    const [modalVisible, setModalVisble] = useState(false)
    const [itemIndex, setItemIndex] = useState(null);
    const [isImagePressed, setIsImagePressed] = useState(true);


    useEffect(() => {
        setLoading(true)
        fetch('https://rozgaarindia.com/bridge/workDetails', {
            method: 'POST',
            timeout: 10000,
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Auth-Token': 'Ezxe01MBXU2werWrW2Wi342ASDADAShyIIoKvmYI'
            },
            body: JSON.stringify({
                "post_token": "162166816853",
                "type": "get",

            })
        })
            .then((response) => response.json())
            // .then((json) => console.log(json))
            .then((json) => {
                if (json.status === 'success') {
                    setTopItems(json.data.portfolio_images)
                }
            })
            .catch((error) => (error))
            .finally(() => setLoading(false));
    }, []);



    const indexIncrease = (count) => {
        let newCount = count + 1
        let totalImages = topItems.length
        return (
            <Text>{newCount}/ {totalImages}  </Text>
        )
    }
  
    const ViewSwitch = (count, path, type) => {

        switch (type) {
            case "img":
                {
                    return (
                        <View style={styles.cardView} >
                            <View style={styles.imageIndex}>
                                <Text style={styles.imageNumber}>{indexIncrease(count)} </Text>
                            </View>
                            <View style={styles.caraouselView}>
                                <Image style={styles.image} source={{ uri: path }} />
                            </View>
                        </View>
                    )
                }
            case "av":
                {
                    return (<View style={styles.cardView} >
                        <Video source={{ uri: item.path }} isPlaying={false} resizeMode={'contain'}
                            useNativeControls style={styles.image} />
                    </View>
                    )
                }
            default:
                break;
        }
    }
    
    return !isImagePressed ? (
      <SafeAreaView style={styles.modalImage}>
        <FlatList
          data={topItems}
          keyExtractor={(item) => item.keyIndex}
          initialScrollIndex={itemIndex}
          horizontal
          pagingEnabled
          scrollEnabled
          snapToAlignment="center"
          scrollEventThrottle={16}
          decelerationRate={"fast"}
          showHorizontalScrollIndicator={false}
          renderItem={({ item, index }) => (
            <View>
              <TouchableOpacity
                onPress={() => setIsImagePressed(true)}
                style={styles.closeIcon}
              >
                <Entypo name="cross" size={24} color="white" />
              </TouchableOpacity>
              <View style={styles.cardView1}>
                <PinchToZoom uri={item.uri} />
              </View>
            </View>
          )}
        />
      </SafeAreaView>
    ) : (
      <View style={styles.flatlistmainView}>
        <ScrollView horizontal={true}>
          <View>
            <FlatList
              data={topItems}
              keyExtractor={(item) => item.keyIndex}
              // getItemLayout
              horizontal
              pagingEnabled
              scrollEnabled
              // initialScrollIndex={10}
              snapToAlignment="center"
              scrollEventThrottle={10}
              decelerationRate={"fast"}
              showHorizontalScrollIndicator={false}
              renderItem={({ item, index }) => (
                <View style={styles.flatlistRenderView}>
                  <TouchableOpacity
                    onPress={() => {
                      setIsImagePressed(false);
                      setItemIndex(index);
                    }}
                  >
                    {ViewSwitch(index, item.uri, item.uri_type)}
                  </TouchableOpacity>
                </View>
              )}
            />
            {/* <View
              style={{
                width: [width],
                height: 2,
                backgroundColor: [inactiveColor],
                overflow: hidden,
              }}
            >
              <View
                style={{
                  position: "absolute",
                  top: 0,
                  left: 0,
                  width: [width],
                  backgroundColor: [activeColor],
                }}
              />
            </View> */}
          </View>

          <View style={styles.iconView}>
            <TouchableOpacity
              onPress={() => navigation.navigate("Portfolio")}
              style={styles.arrowIcon}
            >
              <AntDesign name="arrowright" size={50} color="#0000FF" />
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
}


const styles = StyleSheet.create({
    cardView: {
        width: width,
        height: height / 3,
        backgroundColor: 'white',
        borderRadius: 10,
        shadowColor: '#000',
        shadowOffset: { width: 0.5, height: 0.5 },
        shadowOpacity: 0.5,
        shadowRadius: 3,
    },
    image: {
        resizeMode: 'cover',
        width: width,
        height: height / 3,
        borderRadius: 10,
        flex: 1

    },
    imageIndex: {
        alignSelf: 'flex-end',
        color: 'white',
        position: 'absolute',
        zIndex: 1,
        // elevation : 1,
        backgroundColor: '#e5e4e2',
        borderRadius: 100,
        alignContent: 'center',
        marginTop: 5
    },
    imageNumber: {
        color: 'grey',
        paddingLeft: 5,
    },
    caraouselView: {
        flex: 1
    },
    flatlistRenderView: {
        flex: 1
    },
    flatlistmainView: {
        flex: 1
    },
    modalClose: {
        color: 'white'
    },
    modalImage: {
        alignSelf: 'center',
        flex: 1,
        justifyContent: 'center',
        backgroundColor: "black",
        width: width,
        height: height / 3,

    },
    arrowIcon: {
        marginTop: 52,
        alignItems: 'center',
        justifyContent : 'center',
        borderRadius: 100,
        shadowOffset : {
            width :3,
            height :3,
        } ,
        shadowRadius : 10,
        shadowOpacity : 0.1,
        width : 100 ,
        height : 150,
        backgroundColor: 'white',

    },
    iconView :{
        flex : 1,
        width : width,
        alignItems : 'center'
    },
    cardView1 : {
        flex : 1,
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems : "center",
       
        width: width,
        height: height / 2,

    },
    
   
    closeIcon: {
     alignItems : 'flex-end',

    }

})

export default Caraousel