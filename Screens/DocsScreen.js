import React ,{useState , useEffect} from 'react'
import { View, Text, FlatList } from 'react-native'
import { MaterialCommunityIcons } from "@expo/vector-icons";

const DocsScreen = () => {
    
    const [documents , setDocuments] = useState([])
    const [loading, setLoading] = useState()
    let dcs = [];

    useEffect(() => {

        setLoading(true)
        fetch('https://www.rozgaarindia.com/bridge/portfolioWorkImages', {
            method: 'POST',
            timeout: 10000,
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Auth-Token': 'Ezxe01MBXU2werWrW2Wi342ASDADAShyIIoKvmYI'
            },
            body: JSON.stringify({
                "post_token": '162442856388',
                "type": "get",

            })
        })
            .then((response) => response.json())
            // .then((json) => console.log(json))
            .then((json) => {
                if (json.status === 'success') {
                    setDocuments(json.data.portfolio_images)
                }
            })
            .catch((error) => (error))
            .finally(() => setLoading(false));

    }, []);
    const ViewDoc = (index,path, type) => {
    
        switch (type) {
            case "others":
                {
                    let extension = path.split(".").pop();
                    switch (extension) {
                      case "pdf":
                        return (
                          <View>
                            <MaterialCommunityIcons
                              name="pdf-box"
                              size={120}
                              color="#808080"
                            />
                          </View>
                        );
                        case "doc" : 
                        return (
                          <View>
                            <MaterialCommunityIcons
                              name="file-word"
                              size={120}
                              color="#808080"
                            />
                          </View>
                        );
                        case "pptx" : {
                            <View>
                              <MaterialCommunityIcons
                                name="file-powerpoint"
                                size={120}
                                color="#808080"
                              />
                            </View>;
                        }

                      default:
                        break;
                    }
        
                }
              
            default:
                break;
        }
    }
    return (
        <View >
            {
                documents.map((item) => {
                    if (item.portfoli_img_type == "others") {
                        dcs.push(item)
                    }
                })
            }

           <FlatList
           data = {dcs}
           numColumns = {3}
           keyExtractor = {item => item.keyIndex}
           renderItem = {({ item , index }) => (
               <View style ={{flex :1}}>
                   {ViewDoc(index, item.portfoli_img_url, item.portfoli_img_type)}
               </View>
           )}
           />
        </View>
    )
}

export default DocsScreen
