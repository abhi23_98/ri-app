import React from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  FlatList,
  Image,
  Dimensions
} from "react-native";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import ImageScreen from '../Screens/ImageScreen'
import VideoScreen from '../Screens/VideoSceen'
import DocsScreen from '../Screens/DocsScreen'
// import { SafeAreaView } from "react-native";
// import { MaterialCommunityIcons } from "@expo/vector-icons";
// import { Video } from "expo-av";
// import PinchToZoom from "../Components/PinchToZoom";
// import { Entypo } from '@expo/vector-icons';

const Tab = createMaterialTopTabNavigator();
// const width = Dimensions.get('window').width;
// const height = Dimensions.get('window').height;

const Portfolio = () => {


  return (
    <View style={{ flex: 1 }}>

  <View style={styles.buttonView}>
  <Tab.Navigator>
    <Tab.Screen name="Images" component={ImageScreen} />
    <Tab.Screen name="Video" component={VideoScreen} />
    <Tab.Screen name="Documents" component={DocsScreen} />
  </Tab.Navigator>
</View>

</View>
);
};
 
//   const [loading, setLoading] = useState(false);
//   const [data, setData] = useState([]);
//   const [index, setIndex] = useState(null);
//   const [images, setImages] = useState([]);
//   const [MyState, setMyState] = useState(true);
//   const [status, setStatus] = useState({});
//   const [displayImage, setDisplayImage] = useState(null)
// ;

  // let imgs = [];
  // let vids = [];
  // let dcs = [];
  
  // useEffect(() => {
  //   setLoading(true);
  //   fetch("https://www.rozgaarindia.com/bridge/portfolioWorkImages", {
  //     method: "POST",
  //     timeout: 10000,
  //     headers: {
  //       Accept: "application/json",
  //       "Content-Type": "application/json",
  //       "Auth-Token": "Ezxe01MBXU2werWrW2Wi342ASDADAShyIIoKvmYI",
  //     },
  //     body: JSON.stringify({
  //       post_token: "162442856388",
  //       type: "get",
  //     }),
  //   })
  //     .then((response) => response.json())
  //     // .then((json) => console.log(json))
  //     .then((json) => {
  //       if (json.status === "success") {
  //         setData(json.data.portfolio_images);
  //       }
  //     })
  //     .catch((error) => error)
  //     .finally(() => setLoading(false));
  // }, []);

  // const ViewImage = (index, path, type) => {
  //   switch (type) {
  //     case "img": {
  //       return (
  //         <View style={styles.cardView}>
  //           <TouchableOpacity
  //             onPress={() => {
  //               setMyState(false);
  //               setIndex(index);
  //               setDisplayImage(path);
  //             }}
  //           >
  //             <Image style={styles.image} source={{ uri: path }} />
  //           </TouchableOpacity>
  //         </View>
  //       );
  //     }
  //     case "av": {
  //       return (
  //         <View style={styles.cardView}>
  //           <TouchableOpacity onPress={() => setMyState(false)}>
  //             <Video
  //               source={{ uri: path }}
  //               isPlaying={true}
  //               resizeMode={"contain"}
  //               isLooping={false}
  //               useNativeControls
  //               style={styles.image}
  //             />
  //           </TouchableOpacity>
  //         </View>
  //       );
  //     }
  //     case "others": {
  //       let extension = path.split(".").pop();
  //       // console.log(extension);
  //       switch (extension) {
  //         case "pdf":
  //           return (
  //             <View>
  //               <MaterialCommunityIcons
  //                 name="pdf-box"
  //                 size={120}
  //                 color="#808080"
  //               />
  //             </View>
  //           );

  //         case "doc":
  //           return (
  //             <View>
  //               <MaterialCommunityIcons
  //                 name="file-word"
  //                 size={120}
  //                 color="#808080"
  //               />
  //             </View>
  //           );

  //         case "pptx":
  //           return (
  //             <View>
  //               <MaterialCommunityIcons
  //                 name="file-powerpoint"
  //                 size={120}
  //                 color="#808080"
  //               />
  //             </View>
  //           );

  //         default:
  //           break;
  //       }
  //     }
  //     default:
  //       break;
  //   }
  // };

  // const Images = () => {
  //   return (
  //     <FlatList
  //       data={imgs}
  //       numColumns={3}
  //       keyExtractor={(item) => item.keyIndex}
  //       decelerationRate = {'fast'}
  //       renderItem={({ item, index }) => (
  //         <View>
  //             {ViewImage(index, item.portfoli_img_url, item.portfoli_img_type)}
  //         </View>
  //       )}
  //     />
  //   );
  // };
  // const Videos = () => {
  //   return (
  //     <FlatList
  //       data={vids}
  //       numColumns={3}
  //       keyExtractor={(item) => item.keyIndex}
  //       renderItem={({ item, index }) => (
  //         <View>
  //             {ViewImage(index, item.portfoli_img_url, item.portfoli_img_type)}
  //         </View>
  //       )}
  //     />
  //   );
  // };
  // const Docs = () => {
  //   return (
  //     <FlatList
  //       data={dcs}
  //       numColumns={3}
  //       keyExtractor={(item) => item.keyIndex}
  //       renderItem={({ item, index }) => (
  //         <View>
  //           {ViewImage(index, item.portfoli_img_url, item.portfoli_img_type)}
  //         </View>
  //       )}
  //     />
  //   );
  // };


 
      {/* {data.map((item) => {
        switch (item.portfoli_img_type) {
          case "img":
            imgs.push(item);
            break;
          case "av":
            vids.push(item);
            break;
          case "others":
            dcs.push(item);
          default:
            break;
        }
      })} */}
      
      {/* !MyState ? (
        <SafeAreaView style={styles.safeAreaView}>
          <TouchableOpacity onPress={()=>{setMyState(true)}} style={{alignSelf:'flex-end', paddingRight:4}}>
          <Entypo name="cross" size={24} color="white" />
          </TouchableOpacity>
          <Image style={styles.bigImage} source={{ uri: displayImage }}
          resizeMode='contain' />
        </SafeAreaView>
      ) : ( */}
     

const styles = StyleSheet.create({
  buttonView: {
    flex: 1,
  },
  toggleButtonText: {
    textAlign: "center",
   
  },
  cardView: {
    flex: 1,
    width: "100%",
    height: "100%",
    flexDirection: "row",
  },
  bigImage:{
    width:'100%',
    height:'100%'

  },
  image: {
    margin: 8,
    width: 100,
    height: 100,
    borderRadius: 10,
    flexDirection: "row",
    alignItems : 'center'
  },
  safeAreaView : {
    flex: 1, 
    backgroundColor: "black"
  }
  // docIcons : {
  //   flexDirection : 'row',
  //   alignItems : 'center'
  // }
});

export default Portfolio;
