import React, { useState, useEffect } from 'react'
import { TouchableOpacity } from 'react-native';
import { ScrollView } from 'react-native';
import { View, Text, Dimensions, Image, FlatList, StyleSheet, Modal } from 'react-native'
import PinchToZoom from '../Components/PinchToZoom';



const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const ImageScreen = (props) => {
    const [loading, setLoading] = useState();
    const [images, setImages] = useState([]);
    // const [modalVisible, setModalVisble] = useState(false)
    // const [imageIndex, setImageIndex] = useState(null)
    let imgs = [];
    
    //const {value} = route.params
   
    useEffect(() => {
        // console.log(temp);
        setLoading(true)
        fetch('https://www.rozgaarindia.com/bridge/portfolioWorkImages', {
            method: 'POST',
            timeout: 10000,
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Auth-Token': 'Ezxe01MBXU2werWrW2Wi342ASDADAShyIIoKvmYI'
            },
            body: JSON.stringify({
                "post_token": '162442856388',
                "type": "get",

            })
        })
            .then((response) => response.json())
            //.then((json) => console.log(json))
            .then((json) => {
                if (json.status === 'success') {
                    setImages(json.data.portfolio_images)
                    // image = images.filter((item)=> item.portfoli_img_type == 'img')

                }
            })
            .catch((error) => (error))
            .finally(() => setLoading(false));

    }, []);




    const ViewImage = (index, path, type) => {

        switch (type) {
            case "img":
                {

                    return (
                        <View style={styles.cardView}>
                            <Image style={styles.image} source={{ uri: path }} />
                        </View>
                    )
                }
          
            default:
                break;
        }
    }
    const ViewImageNew = (index, path, type) => {

        switch (type) {
            case "img":
                {

                    return (
                        <View style={styles.cardView1} >

                            <View style={{ flex: 1 }}>
                                <Image style={styles.image1} source={{ uri: path }} />
                            </View>
                        </View>
                    )
                }
            // case  "av" : {
            //     return
            // }
            // case  "others" : {
            //     return
            // }
            default:
                break;
        }
    }

    return (
        <View >

            {
                images.map((item) => {
                    if (item.portfoli_img_type == 'img') {
                        imgs.push(item)
                    }
                })
            }
            <FlatList
                data={imgs}
               
                numColumns={3}
                keyExtractor={item => item.keyIndex}
                renderItem={({ item, index }) => (
                    <View>
                        <TouchableOpacity onPress={() => { setModalVisble(true); setImageIndex(index) }}>
                            {ViewImage(index, item.portfoli_img_url, item.portfoli_img_type)}
                        </TouchableOpacity>

                    </View>
                )}

            />
            {/* <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}

            >
                <View style={styles.modalImage}>
                
                     <FlatList
                        data={imgs}
                        // contentContainerStyle={{justifyContent:'center'}}
                        keyExtractor={item => item.keyIndex}
                        horizontal
                        pagingEnabled
                        scrollEnabled
                        initialScrollIndex={imageIndex}
                        snapToAlignment='center'
                        // scrollEventThrottle={16}
                        decelerationRate={"fast"}
                        showHorizontalScrollIndicator={false}
                        renderItem={({ item, index }) => (
                            <View>

                                {ViewImageNew(index, item.portfoli_img_url, item.portfoli_img_type)}

                                <View>
                                    <TouchableOpacity onPress={() => setModalVisble(false)}>
                                        <Text style={{ color: 'white' }}> Close</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        )}

                    /> 
                     <View> 
                         </View>
 
                </View>



            </Modal> */}
        </View>
    )
}

export default ImageScreen

const styles = StyleSheet.create({

    cardView: {
        flex: 1,
        width: '100%',
        height: '100%',
        flexDirection: 'row'


    },
    image: {
        margin: 8,
        width: 100,
        height: 100,
        borderRadius: 10,
        flexDirection: 'row',
    },
    modalImage: {
        alignSelf: 'center',
        flex: 1,
        justifyContent: 'center',
        backgroundColor: "black",
        width: '100%',
        height: '100%',
        

    },
    cardView1: {
        width: width,
        height: height / 3,
        backgroundColor: 'white',
        borderRadius: 10,
        shadowColor: '#000',
        shadowOffset: { width: 0.5, height: 0.5 },
        shadowOpacity: 0.5,
        shadowRadius: 3,
        marginTop: 240
    },
    image1: {
        resizeMode: 'cover',
        width: width,
        height: height / 3,
        borderRadius: 10,
        // marginTop : 100

    },
})
