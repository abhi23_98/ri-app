import React from 'react'
import { View, Text , StyleSheet , ScrollView, TouchableOpacity } from 'react-native'

const HomeScreen = ({navigation}) => {
    return (
        <ScrollView style={styles.Contanier} vertical={true}>
        <View style={styles.ScrollView_Screen}>
          <View style={styles.btn_Button}>
            <TouchableOpacity style={styles.bn} onPress={() => navigation.navigate('Caraousel')}>
                  <Text style={styles.text}>Carousel</Text>
              </TouchableOpacity>
              {/* <TouchableOpacity style={styles.bn} onPress={() => navigation.navigate('ImageScreen', {test:'bb'})}>
                  <Text style={styles.text}>Carousel</Text>
              </TouchableOpacity> */}
              <TouchableOpacity style={styles.bn} onPress={() => navigation.navigate('Portfolio')}>
                  <Text style={styles.text}>Portfolio</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.bn} onPress={() => navigation.navigate('Expert')}>
                  <Text style={styles.text}>Expert</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.bn} onPress={() => navigation.navigate('GlobalHrTrends')}>
                  <Text style={styles.text}>GlobalHrTrends</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.bn} onPress={() => navigation.navigate('RecruitmentConsultants')}>
                  <Text style={styles.text}>RecruitmentConsultants</Text>
              </TouchableOpacity>
              </View>
              </View>
              </ScrollView>
    )
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    bn : {
      padding :10,
      backgroundColor :'#0000ff',
      width: 200,
      margin:10,
      
    },
    btn_Button : {
       alignSelf : 'center',
       marginTop : 100
    },
    text : {
      fontSize : 20,
      color : "#fff",
      textAlign : 'center'
    }
  });


  export default HomeScreen

