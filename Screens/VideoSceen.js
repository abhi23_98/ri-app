import React, { useState , useEffect } from 'react'
import { View, Text , StyleSheet, FlatList } from 'react-native'
import { Video } from 'expo-av';

const VideoSceen = () => {

    const [videos , setVideos] = useState([])
    const [loading, setLoading] = useState([])
    const vids = [];

    useEffect(() => {

        setLoading(true)
        fetch('https://www.rozgaarindia.com/bridge/portfolioWorkImages', {
            method: 'POST',
            timeout: 10000,
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Auth-Token': 'Ezxe01MBXU2werWrW2Wi342ASDADAShyIIoKvmYI'
            },
            body: JSON.stringify({
                "post_token": '162442856388',
                "type": "get",

            })
        })
            .then((response) => response.json())
            //.then((json) => console.log(json))
            .then((json) => {
                if (json.status === 'success') {
                    setVideos(json.data.portfolio_images)
                }
            })
            .catch((error) => (error))
            .finally(() => setLoading(false));

    }, []);
    const ViewVid = (index,path, type) => {
    
            switch (type) {
                case "av":
                    {
            
                        return (
                            <View style={styles.cardView} >
                                <Video source={{ uri: path }} isPlaying={true} resizeMode={'contain'}
                                    useNativeControls style={styles.image} />
                            </View>
                        )
                    }
                  
                default:
                    break;
            }
        }

    return (
        <View >
             {
             videos.map((item)=>{
                if(item.portfoli_img_type == 'av'){
                    vids.push(item)
                }
             })
           
         }
              <FlatList
                data={vids}
                // contentContainerStyle={{justifyContent:'center'}}
                numColumns={3}
                keyExtractor={item => item.keyIndex}
                renderItem={({ item, index }) => (
                    <View  >
                        {ViewVid(index, item.portfoli_img_url, item.portfoli_img_type)}
                    </View>
                )}

            />
        </View>
    )
}

export default VideoSceen
const styles = StyleSheet.create({
   
    cardView : {
        flex :1,
        width : '100%',
        height : '100%',


       
    },
    image : {
       margin:8,
       width : 100,
       height : 100,
       borderRadius : 10,
  

       
    
    }
})