// import React, { useState, useEffect } from "react";
// import { useNavigation } from "@react-navigation/native";
// import {
//   StyleSheet,
//   View,
//   Text,
//   Modal,
//   FlatList,
//   Image,
//   Animated,
//   SafeAreaView,
//   TouchableOpacity,
//   ScrollView,
// } from "react-native";
// import { FontAwesome, FontAwesome5, Entypo } from "@expo/vector-icons";
// import ImageViewer from "react-native-image-zoom-viewer";
// import Caraousel from "./Caraousel";
// import PinchToZoom from "./PinchToZoom";

// const Screen = () => {
//   const navigation = useNavigation();
//   const [loading, setLoading] = useState();
//   const [workItems, setWorkItems] = useState();
//   const [showModal, setShowModal] = useState(false);
//   const [index, setIndex] = useState(null);
//   const [isImagePressed, setIsImagePressed] = useState(false);

//   useEffect(() => {
//     setLoading(true);
//     fetch("https://rozgaarindia.com/bridge/workDetails", {
//       method: "POST",
//       timeout: 10000,
//       headers: {
//         Accept: "application/json",
//         "Content-Type": "application/json",
//         "Auth-Token": "Ezxe01MBXU2werWrW2Wi342ASDADAShyIIoKvmYI",
//       },
//       body: JSON.stringify({
//         post_token: "162166816853",
//         type: "get",
//       }),
//     })
//       .then((response) => response.json())
//       //.then((json) => console.log(json))
//       .then((json) => {
//         if (json.status === "success") {
//           setWorkItems(json.data.portfolio_images);
//         }
//       })
//       .catch((error) => error)
//       .finally(() => setLoading(false));
//   }, []);

//   return isImagePressed ? (
//     <SafeAreaView style={{ flex: 1, backgroundColor: "black" }}>
//       <TouchableOpacity
//         style={styles.crossBtn}
//         onPress={() => {
//           setIsImagePressed(false);
//         }}
//       >
//         <Entypo name="circle-with-cross" size={30} color="white" />
//       </TouchableOpacity>
//       <Caraousel data={workItems} index={index} />
//     </SafeAreaView>
//   ) : (
//     <SafeAreaView style={styles.mainContainer}>
//       <View style={styles.header}>
//         <Text style={styles.titleText}>Work Samples</Text>
//         <TouchableOpacity>
//           <FontAwesome name="plus" size={30} color="#1493e3" />
//         </TouchableOpacity>
//       </View>

//       <View style={styles.scrollBody}>
//         <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
//           <FlatList
//             data={workItems}
//             contentContainerStyle={styles.itemsWrapper}
//             horizontal
//             scrollEnabled={false}
//             keyExtractor={(item) => item.keyIndex}
//             renderItem={({ item, index }) => {
//               return (
//                 <TouchableOpacity
//                   onPress={() => {
//                     setIsImagePressed(true);
//                     setIndex(index);
//                   }}
//                 >
//                   <Image style={styles.items} source={{ uri: item.uri }} />
//                 </TouchableOpacity>
//               );
//             }}
//           />
//           <TouchableOpacity style={styles.rightIcon}>
//             <FontAwesome5 name="angle-double-right" size={50} color="#1493e3" />
//           </TouchableOpacity>
//         </ScrollView>
//       </View>
//       <Modal
//         transparent={false}
//         visible={showModal}
//         onRequestClose={() => {
//           setShowModal(!showModal);
//         }}
//       >
//         <Animated.View>
//           <PinchToZoom uri="https://res.cloudinary.com/rozgaarindia/image/upload/w_800,c_scale,q_90/v1621668316/portfolio/phpUre5gx_t4heyy.png" />
//         </Animated.View>
//         {/ <ImageViewer imageUrls = {images} /> /}
//         {/* <View style={styles.mediaGallery}>
//               <TouchableOpacity
//                 style={styles.crossBtn}
//                 onPress={() => {
//                   setShowModal(!showModal);
//                 }}
//               >
//                 <Entypo name="circle-with-cross" size={30} color="white" />
//               </TouchableOpacity>
//               <View style={{ flex: 1, marginTop: 40 }}> */}
//         {/ <Caraousel data={workItems} index={index}/> /}
//         {/ <PinchToZoom uri="https://res.cloudinary.com/rozgaarindia/image/upload/w_800,c_scale,q_90/v1621668316/portfolio/phpUre5gx_t4heyy.png"/> /}
//         {/* </View>
//             </View> */}
//       </Modal>
//     </SafeAreaView>
//   );
// };



// const styles = StyleSheet.create({
//   mainContainer: {
//     flex: 1,
//   },
//   itemsWrapper: {
//     justifyContent: "space-between",
//   },
//   items: {
//     flexDirection: "row",
//     width: 180,
//     height: 150,
//     borderRadius: 7,
//     margin: 10,
//   },
//   titleText: {
//     fontSize: 20,
//     fontWeight: "bold",
//   },
//   header: {
//     padding: 15,
//     flexDirection: "row",
//     justifyContent: "space-between",
//   },
//   scrollBody: {
//     flexDirection: "row",
//   },
//   rightIcon: {
//     width: 180,
//     height: 150,
//     justifyContent: "center",
//     alignItems: "center",
//   },
//   mediaGallery: {
//     backgroundColor: "black",
//     flex: 1,
//     alignItems: "center",
//   },
//   crossBtn: {
//     alignSelf: "flex-end",
//     padding: 15,
//   },
//   headerText: {
//     color: "white",
//     fontSize: 32,
//     marginTop: 50,
//     marginBottom: 25,
//   },
// });
// export default Screen;