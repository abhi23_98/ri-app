import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { StyleSheet, Text, View } from 'react-native'
import HomeScreen from '../Screens/HomeScreen'
import ProfileScreen from '../Screens/ProfileScreen'
import Caraousel from '../Components/Caraousel'
import Portfolio from '../Screens/Portfolio'
// import ImageScreen from '../Screens/ImageScreen'
import Expert from '../Components/Expert'
import GlobalHrTrends from '../Components/GlobalHrTrends'
import RecruitmentConsultants from '../Components/RecruitmentConsultants'


const Stack = createStackNavigator();

const AppNavigation = () => {
    return (
        <NavigationContainer independent={true}>
            <Stack.Navigator>
                <Stack.Screen name='HomeScreen' component={HomeScreen}/>
                <Stack.Screen name='ProfileScreen' component={ProfileScreen}/>
                <Stack.Screen name='Caraousel' component={Caraousel}/>
                <Stack.Screen name='Portfolio' component={Portfolio}/>
                <Stack.Screen name='Expert' component={Expert}/>
                <Stack.Screen name='GlobalHrTrends' component={GlobalHrTrends}/>
                <Stack.Screen name='RecruitmentConsultants' component={RecruitmentConsultants}/>
                {/* <Stack.Screen name='Screen' component={Screen}/> */}
            </Stack.Navigator>
        </NavigationContainer>
    )
}

export default AppNavigation

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });

