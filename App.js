import React from 'react';
import { StyleSheet } from 'react-native';
import AppNavigation from './Navigation/AppNavigation'
import {NavigationContainer} from '@react-navigation/native'

const App = () => {
  return (
    <NavigationContainer>
      <AppNavigation />
    </NavigationContainer>

  )
}
    export default App

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

